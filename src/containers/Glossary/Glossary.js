import React, {useEffect, useState} from 'react';
import axios from 'axios';
import Country from "../../components/Country/Country";
import './Glossary.css';
import CountryInfo from "../../components/CountryInfo/CountryInfo";

const Glossary = () => {
    const [countries, setCountries] = useState([]);
    const [selectedCountryId, setSelectedCountryId] = useState(null);
    const [countryInfo, setCountryInfo] = useState(null);
    const [countryBorders, setCountryBorders] = useState(null);

    useEffect(() => {
        const newCountryInfo = countries[selectedCountryId];
        setCountryInfo(newCountryInfo);
        const fetchData = async () => {
                const responses = await Promise.all(newCountryInfo.borders.map(code => axios.get('https://restcountries.eu/rest/v2/alpha/' + code)));
                setCountryBorders(responses.map(item => {
                    return item.data;
                }));
        };
        if (newCountryInfo && newCountryInfo.borders && newCountryInfo.borders.length > 0) {
            fetchData();
        }
    }, [selectedCountryId, countries])

    useEffect(() => {
        const showCountries = async (e) => {
            const response = await axios.get('https://restcountries.eu/rest/v2/all');
            setCountries(response.data);
        }
        showCountries();
    }, []);

    return (
        <>
            <section className="Countries">
                {countries.map((country, index) => (
                    // <span>id{index}:{country.name}<br/></span>
                    <Country
                        key={index}
                        index={index}
                        title={country.name}
                        onClick={() => setSelectedCountryId(index)}
                    />
                ))}
            </section>
            <section className="CountryInfo">
                <CountryInfo countryInfo={countryInfo} countryBorders={countryBorders} />
            </section>
        </>
    )
};

export default Glossary;