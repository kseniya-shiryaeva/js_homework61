import './App.css';
import Glossary from "./containers/Glossary/Glossary";

function App() {
  return (
    <Glossary />
  );
}

export default App;
