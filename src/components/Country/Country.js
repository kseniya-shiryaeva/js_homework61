import React from 'react';
import './Country.css';

const Country = props => {
    return (
        <div className="Country" id={props.index} onClick={props.onClick}>
            {props.title}
        </div>
    );
};

export default Country;