import React from 'react';
import './CountryInfo.css';

const CountryInfo = ({countryInfo, countryBorders}) => {

    let infoBlock = 'Выберите страну';

    if (countryInfo) {
        infoBlock = <ul>
            <li><b>Название:</b> {countryInfo.name}</li>
            <li><b>Столица:</b> {countryInfo.capital}</li>
            <li><b>Население:</b> {countryInfo.population}</li>
        </ul>;
    }

    let bordersInfo = null;

    if (countryBorders) {
        bordersInfo = <div>
            <h3>Границы:</h3>
            <ul>
                {countryBorders.map(item => (
                    <li key={Math.random()}>{item.name}</li>
                ))}
            </ul>
        </div>;
    }

    return (
        <div className="CountryInfo">
            {infoBlock}
            {bordersInfo}
        </div>
    );
};

export default CountryInfo;